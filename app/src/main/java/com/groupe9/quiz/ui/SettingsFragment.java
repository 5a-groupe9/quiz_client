package com.groupe9.quiz.ui;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.groupe9.quiz.R;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private Button bt_video;
    private VideoView videoView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.settings_fragment, container, false);
        bt_video = root.findViewById(R.id.bt_video);
        bt_video.setOnClickListener(this);
        videoView = root.findViewById(R.id.videoView);
        videoView.setVideoURI(Uri.parse("https://rickrolled.fr/rickroll.mp4"));
        videoView.requestFocus();
        return root;
    }

    @Override
    public void onClick(View view) {
        if (view == bt_video) {
            videoView.setVisibility(View.VISIBLE);
            bt_video.setVisibility(View.GONE);
            videoView.start();
        }
    }
}