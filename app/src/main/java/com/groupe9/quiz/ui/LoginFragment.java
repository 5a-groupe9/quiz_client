package com.groupe9.quiz.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.groupe9.quiz.HomeActivity;
import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.RetrofitService;
import com.groupe9.quiz.webservice.pojo.Difficulty;
import com.groupe9.quiz.webservice.service.SoloGameService;

import java.util.List;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment implements View.OnClickListener {

    private Button bt_connect;
    private Button bt_register;
    private ProgressBar pg_loading;
    private TextInputEditText et_username;
    private TextInputEditText et_password;
    private Activity activity;

    private final static String AUTH_TOKEN = "AUTH_TOKEN";

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        activity = getActivity();
        pg_loading = view.findViewById(R.id.pg_loading);
        bt_connect = view.findViewById(R.id.bt_connect);
        bt_register = view.findViewById(R.id.bt_register);
        et_username = view.findViewById(R.id.et_username);
        et_password = view.findViewById(R.id.et_password);
        bt_connect.setOnClickListener(this);
        bt_register.setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setEnableForm(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_connect:
                if (isValidForm()) {
                    setEnableForm(false);
                    String token = Credentials.basic(et_username.getText().toString(), et_password.getText().toString());
                    login(token);
                }
                break;
            case R.id.bt_register:
                getParentFragmentManager().beginTransaction().replace(R.id.fragment, new RegisterFragment()).addToBackStack(null).commit();
                break;
            default:
                break;
        }
    }

    private boolean isValidForm() {
        if (TextUtils.isEmpty(et_username.getText())) {
            et_username.setError(getString(R.string.empty_field));
            return false;
        } else if (TextUtils.isEmpty(et_password.getText())) {
            et_password.setError(getString(R.string.empty_field));
            return false;
        }
        return true;
    }

    private void setEnableForm(boolean enableForm) {
        et_username.setEnabled(enableForm);
        et_password.setEnabled(enableForm);
        bt_connect.setEnabled(enableForm);
        bt_register.setEnabled(enableForm);
        if (enableForm) {
            pg_loading.setVisibility(View.INVISIBLE);
        } else {
            pg_loading.setVisibility(View.VISIBLE);
        }
    }

    private void login(String token) {
        SoloGameService soloGameService = RetrofitService.createService(SoloGameService.class, token);
        Call<List<Difficulty>> call;
        call = soloGameService.getDifficulties();
        call.enqueue(new Callback<List<Difficulty>>() {
            @Override
            public void onResponse(@NonNull Call<List<Difficulty>> call, @NonNull Response<List<Difficulty>> response) {
                if (response.isSuccessful()) {
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.putExtra(AUTH_TOKEN, token);
                    startActivity(intent);
                } else {
                    activity.runOnUiThread(() -> {
                        Toast.makeText(getContext(), getString(R.string.incorrect_credentials), Toast.LENGTH_LONG).show();
                        setEnableForm(true);
                        et_password.setText("");
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Difficulty>> call, @NonNull Throwable t) {
                View view = getView();
                if (view != null) {
                    activity.runOnUiThread(() -> {
                        Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show();
                        setEnableForm(true);
                    });
                }
            }
        });
    }

}