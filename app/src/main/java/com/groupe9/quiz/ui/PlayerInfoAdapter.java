package com.groupe9.quiz.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.message.answer.PlayerAndScore;

import java.util.List;

public class PlayerInfoAdapter extends RecyclerView.Adapter<PlayerInfoAdapter.ViewHolder> {

    private List<PlayerAndScore> playerAndScoreList;

    public PlayerInfoAdapter(List<PlayerAndScore> playerAndScoreList) {
        this.playerAndScoreList = playerAndScoreList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_playerscore_recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlayerAndScore playerAndScore = playerAndScoreList.get(position);
        holder.tv_playername_and_position.setText(playerAndScore.getName() + " #" + (position + 1));
        holder.tv_playerscore.setText(playerAndScore.getScore() + " pts");
        int combo = playerAndScore.getCombo();
        if (combo == 0) {
            holder.ll_combo.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_combo.setText(String.valueOf(combo));
        }
        /*if (position == 0) {
            holder.cd_recyclerview_item.setBackgroundColor(Color.parseColor("#C9E1CD15"));
        }*/
    }

    @Override
    public int getItemCount() {
        return playerAndScoreList.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_playername_and_position;
        public TextView tv_playerscore;
        public CardView cd_recyclerview_item;
        public TextView tv_combo;
        public LinearLayout ll_combo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_playername_and_position = itemView.findViewById(R.id.tv_playername_and_position);
            tv_playerscore = itemView.findViewById(R.id.tv_playerscore);
            cd_recyclerview_item = itemView.findViewById(R.id.cd_recyclerview_item);
            tv_combo = itemView.findViewById(R.id.tv_combo);
            ll_combo = itemView.findViewById(R.id.combo_linearlayout);
        }
    }
}
