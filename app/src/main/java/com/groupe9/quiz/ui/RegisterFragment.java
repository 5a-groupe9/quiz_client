package com.groupe9.quiz.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.groupe9.quiz.R;
import com.groupe9.quiz.model.User;
import com.groupe9.quiz.webservice.RetrofitService;
import com.groupe9.quiz.webservice.service.PlayerService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private TextInputEditText et_username;
    private TextInputEditText et_password;
    private TextInputEditText et_confirm_password;
    private Button bt_submit_registration;
    private Activity activity;

    public RegisterFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment, container, false);
        activity = getActivity();
        et_username = view.findViewById(R.id.et_username_register);
        et_password = view.findViewById(R.id.et_password_register);
        et_confirm_password = view.findViewById(R.id.et_confirm_password_register);
        bt_submit_registration = view.findViewById(R.id.bt_submit_registration);
        bt_submit_registration.setOnClickListener(this);
        return view;
    }

    private boolean isValidForm() {
        List<TextInputEditText> textInputEditTextList = new ArrayList<>(Arrays.asList(et_username, et_password, et_confirm_password));
        for (TextInputEditText editText : textInputEditTextList) {
            if (TextUtils.isEmpty(editText.getText())) {
                editText.setError(getString(R.string.empty_field));
                return false;
            }
        }
        if (!TextUtils.equals(et_password.getText(), et_confirm_password.getText())) {
            et_confirm_password.setText("");
            et_confirm_password.setError(getString(R.string.different_passwords));
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == bt_submit_registration) {
            if (isValidForm()) {
                PlayerService playerService = RetrofitService.createService(PlayerService.class);
                Call<ResponseBody> call = playerService.createUser(new User(et_username.getText().toString(), et_password.getText().toString()));
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            FragmentManager fragmentManager = getParentFragmentManager();
                            fragmentManager.popBackStack();
                            Toast.makeText(getContext(), getString(R.string.account_created), Toast.LENGTH_LONG).show();
                        } else {
                            activity.runOnUiThread(() -> {
                                Toast.makeText(getContext(), getString(R.string.account_not_created), Toast.LENGTH_LONG).show();
                            });
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        View view = getView();
                        if (view != null) {
                            activity.runOnUiThread(() -> Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show());
                        }
                    }
                });
            }
        }
    }
}