package com.groupe9.quiz.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.message.answer.AnswerList;
import com.groupe9.quiz.webservice.message.answer.AnswerQuiz;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StateFragment extends Fragment {

    private static final String DATA = "data";

    private AnswerQuiz answerQuiz;
    private ProgressBar progressBar;

    public StateFragment() {
        // Required empty public constructor
    }

    public static StateFragment newInstance(AnswerQuiz answerQuiz) {
        StateFragment fragment = new StateFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, answerQuiz);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            answerQuiz = (AnswerQuiz) getArguments().getSerializable(DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.state_fragment, container, false);
        progressBar = view.findViewById(R.id.pb_state);
        RecyclerView recyclerView = view.findViewById(R.id.rv_state);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new PlayerInfoAdapter(answerQuiz.getPlayerAndScore()));
        try {
            AnswerList answerList = answerQuiz.getQuestionByLanguage(getString(R.string.language));
            Toast.makeText(getContext(), getString(R.string.correct_answer) + " : " + answerList.getAnswer(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void setProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }
}