package com.groupe9.quiz.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.groupe9.quiz.R;


public class SoloAnswerFragment extends Fragment implements View.OnClickListener {

    private FragmentActivity activity;
    private Button bt_next_question;
    private Button bt_quit;

    public SoloAnswerFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.solo_answer_fragment, container, false);
        activity = getActivity();
        bt_next_question = view.findViewById(R.id.bt_solo_next_question);
        bt_next_question.setOnClickListener(this);
        bt_quit = view.findViewById(R.id.bt_solo_quit);
        bt_quit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == bt_next_question) {
            getParentFragmentManager().setFragmentResult("questionKey", new Bundle());
            getParentFragmentManager().popBackStack();
        } else if (view == bt_quit) {
            activity.finish();
        }
    }
}