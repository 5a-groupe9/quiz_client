package com.groupe9.quiz.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;

import java.util.List;

public class QuizAnswerAdapter extends RecyclerView.Adapter<QuizAnswerAdapter.ViewHolder> {

    private List<String> possibleAnswers;
    private OnAnswerListener onAnswerListener;

    public interface OnAnswerListener {
        void onClick(String selectedAnswer);
    }

    public QuizAnswerAdapter(List<String> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }

    public void setOnAnswerListener(OnAnswerListener onAnswerListener) {
        this.onAnswerListener = onAnswerListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_answer_recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String selectedAnswer = possibleAnswers.get(position);
        holder.button.setText(selectedAnswer);
        holder.button.setOnClickListener(view -> {
            if(onAnswerListener != null) {
                onAnswerListener.onClick(selectedAnswer);
            }
        });
    }

    @Override
    public int getItemCount() {
        return possibleAnswers.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public Button button;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.bt_recyclerview_item);
        }
    }
}
