package com.groupe9.quiz.ui.leaderboard;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.groupe9.quiz.R;
import com.groupe9.quiz.ui.LeaderboardPlayerAdapter;
import com.groupe9.quiz.webservice.RetrofitService;
import com.groupe9.quiz.webservice.pojo.Leaderboard;
import com.groupe9.quiz.webservice.service.PlayerService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaderboardFragment extends Fragment {

    private final static String AUTH_TOKEN = "AUTH_TOKEN";

    private LeaderboardViewModel leaderboardViewModel;
    private Activity activity;
    private String token = "";

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.leaderboard_fragment, container, false);
        leaderboardViewModel = new ViewModelProvider(this).get(LeaderboardViewModel.class);

        activity = getActivity();
        if (activity != null) {
            Bundle extras = activity.getIntent().getExtras();
            if (extras != null) {
                token = extras.getString(AUTH_TOKEN);
            }
        }

        LeaderboardPlayerAdapter leaderboardPlayerAdapter = new LeaderboardPlayerAdapter();
        leaderboardViewModel.getLeaderBoard().observe(getViewLifecycleOwner(), players -> {
            leaderboardPlayerAdapter.setPlayers(players);
            leaderboardPlayerAdapter.notifyDataSetChanged();
        });

        recyclerView = root.findViewById(R.id.rv_leaderboard);
        progressBar = root.findViewById(R.id.pb_leaderboard);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(leaderboardPlayerAdapter);

        initLeaderBoard();

        return root;
    }

    public void initLeaderBoard() {
        PlayerService playerService = RetrofitService.createService(PlayerService.class, token);
        Call<Leaderboard> call = playerService.getLeaderBoard();
        call.enqueue(new Callback<Leaderboard>() {
            @Override
            public void onResponse(@NonNull Call<Leaderboard> call, @NonNull Response<Leaderboard> response) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    Leaderboard leaderboard = response.body();
                    if (leaderboard != null) {
                        leaderboardViewModel.setPlayers(leaderboard.getPlayers());
                    }
                } else {
                    leaderboardViewModel.setPlayers(new ArrayList<>());
                    activity.runOnUiThread(() -> {
                        Toast.makeText(getContext(), getString(R.string.leaderboard_not_found), Toast.LENGTH_LONG).show();
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<Leaderboard> call, @NonNull Throwable t) {
                View view = getView();
                if (view != null) {
                    activity.runOnUiThread(() -> Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show());
                }
            }
        });
    }
}