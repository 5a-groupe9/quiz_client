package com.groupe9.quiz.ui.leaderboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.groupe9.quiz.webservice.pojo.Player;

import java.util.List;

public class LeaderboardViewModel extends ViewModel {

    private MutableLiveData<List<Player>> players = new MutableLiveData<>();

    public LeaderboardViewModel() {

    }

    public LiveData<List<Player>> getLeaderBoard() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players.setValue(players);
    }
}

