package com.groupe9.quiz.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.message.roombeginningstate.Player;

import java.util.List;

public class WaitingPlayersAdapter extends RecyclerView.Adapter<WaitingPlayersAdapter.ViewHolder> {

    private List<Player> playerList;

    public WaitingPlayersAdapter(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList.clear();
        this.playerList.addAll(playerList);
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.waiting_players_recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Player player = playerList.get(position);
        String level = holder.itemView.getResources().getString(R.string.level) + " " + player.getLevel();
        holder.tv_waiting_playerLevel.setText(level);
        holder.tv_waiting_playerName.setText(player.getName());
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_waiting_playerLevel;
        public TextView tv_waiting_playerName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_waiting_playerLevel = itemView.findViewById(R.id.tv_waiting_playerlevel);
            tv_waiting_playerName = itemView.findViewById(R.id.tv_waiting_playername);
        }
    }
}
