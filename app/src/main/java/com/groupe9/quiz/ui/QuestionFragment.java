package com.groupe9.quiz.ui;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.message.question.QuestionQuiz;
import com.groupe9.quiz.webservice.message.question.QuestionsList;

public class QuestionFragment extends Fragment {

    private static final String QUIZ = "quiz";
    private static final String TIMER = "timer";

    private QuestionQuiz questionQuiz;
    private ProgressBar progressBar;

    private int responseTime;
    private QuizAnswerAdapter.OnAnswerListener onAnswerListener;

    private QuestionFragment() {
        // Required empty public constructor
    }

    public static QuestionFragment newInstance(QuestionQuiz questionQuiz, int responseTime) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putSerializable(QUIZ, questionQuiz);
        args.putInt(TIMER, responseTime / 1000);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionQuiz = (QuestionQuiz) getArguments().getSerializable(QUIZ);
            responseTime = getArguments().getInt(TIMER, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.question_fragment, container, false);
        TextView bt_question = view.findViewById(R.id.tv_question);
        RecyclerView recyclerView = view.findViewById(R.id.rv_answers);
        progressBar = view.findViewById(R.id.pb_quiz);
        startTimer();

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        try {
            QuestionsList questionsList = questionQuiz.getQuestionByLanguage(getString(R.string.language));
            QuizAnswerAdapter quizAnswerAdapter = new QuizAnswerAdapter(questionsList.getContent().getPossibleAnswers());
            quizAnswerAdapter.setOnAnswerListener(onAnswerListener);
            recyclerView.setAdapter(quizAnswerAdapter);
            String txtQuestion = questionsList.getContent().getQuestion() + "\n(" + questionQuiz.getQuestionNumber() + "/" + questionQuiz.getQuestionTotalNumber() + ")";
            bt_question.setText(txtQuestion);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    public void startTimer() {
        int limit = responseTime - 2;
        progressBar.setMax(limit);
        progressBar.setProgress(limit);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = limit; i > 0; i--) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressBar.setProgress(i - 1);
                    if (i == 5) {
                        Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
                        progressDrawable.setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
                        progressBar.setProgressDrawable(progressDrawable);
                    }
                }
            }
        }).start();
    }

    public void setOnAnswerListener(QuizAnswerAdapter.OnAnswerListener onAnswerListener) {
        this.onAnswerListener = onAnswerListener;
    }
}