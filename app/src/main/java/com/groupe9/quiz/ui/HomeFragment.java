package com.groupe9.quiz.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.groupe9.quiz.GameActivity;
import com.groupe9.quiz.HomeActivity;
import com.groupe9.quiz.R;
import com.groupe9.quiz.SoloGameActivity;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private Button bt_multiplayer;
    private Button bt_solo;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.home_fragment, container, false);
        bt_solo = root.findViewById(R.id.bt_solo);
        bt_solo.setOnClickListener(this);
        bt_multiplayer = root.findViewById(R.id.bt_multiplayer);
        bt_multiplayer.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View view) {
        Activity activity = getActivity();
        String token = "";
        if (activity != null) {
            Bundle extras = activity.getIntent().getExtras();
            token = extras != null ? extras.getString(HomeActivity.AUTH_TOKEN) : "";
        }
        if (view == bt_multiplayer) {
            Intent intent = new Intent(activity, GameActivity.class);
            intent.putExtra(HomeActivity.AUTH_TOKEN, token);
            startActivity(intent);
        } else if(view == bt_solo) {
            Intent intent = new Intent(activity, SoloGameActivity.class);
            intent.putExtra(HomeActivity.AUTH_TOKEN, token);
            startActivity(intent);
        }
    }
}