package com.groupe9.quiz.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.pojo.Player;

import java.util.ArrayList;
import java.util.List;

public class LeaderboardPlayerAdapter extends RecyclerView.Adapter<LeaderboardPlayerAdapter.ViewHolder> {

    private List<Player> players;

    public LeaderboardPlayerAdapter() {
        this.players = new ArrayList<>();
    }

    public void setPlayers(List<Player> players) {
        this.players.clear();
        this.players.addAll(players);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Player player = players.get(position);
        String level = holder.itemView.getResources().getString(R.string.level) + " " + player.getLevel();
        holder.level.setText(level);
        holder.playerName.setText(player.getName());
        String positionTxt = "#" + (position + 1);
        holder.position.setText(positionTxt);
        String exp = player.getTotalExp() + " exp";
        holder.totalExp.setText(exp);
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView playerName;
        public TextView totalExp;
        public TextView position;
        public TextView level;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            playerName = itemView.findViewById(R.id.tv_playerName_leaderboard);
            totalExp = itemView.findViewById(R.id.tv_exp_leaderboard);
            position = itemView.findViewById(R.id.tv_position_leaderboard);
            level = itemView.findViewById(R.id.tv_level_leaderboard);
        }
    }
}
