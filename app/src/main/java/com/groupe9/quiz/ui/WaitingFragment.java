package com.groupe9.quiz.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.groupe9.quiz.R;
import com.groupe9.quiz.model.util.PlayerDiffUtil;
import com.groupe9.quiz.webservice.message.roombeginningstate.Content;
import com.groupe9.quiz.webservice.message.roombeginningstate.Player;
import com.groupe9.quiz.webservice.message.roombeginningstate.RoomBeginningState;

import java.util.List;

public class WaitingFragment extends Fragment {

    private static final String ROOM_DATA = "room_data";

    private RoomBeginningState roomBeginningState;
    private RecyclerView recyclerView;

    private OnWaiting onWaiting;

    public void setOnWaiting(OnWaiting onWaiting) {
        this.onWaiting = onWaiting;
    }

    public interface OnWaiting {
        void onRequest();
    }

    public WaitingFragment() {
        // Required empty public constructor
    }

    public static WaitingFragment newInstance(RoomBeginningState roomBeginningState) {
        WaitingFragment fragment = new WaitingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ROOM_DATA, roomBeginningState);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            roomBeginningState = (RoomBeginningState) getArguments().getSerializable(ROOM_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.waiting_fragment, container, false);
        Content data = roomBeginningState.getContent();
        recyclerView = view.findViewById(R.id.rv_waiting_players);
        TextView tv_inviteName = view.findViewById(R.id.tv_inviteName);
        TextView tv_category = view.findViewById(R.id.tv_category);
        TextView tv_difficulty = view.findViewById(R.id.tv_difficulty);
        TextView tv_totalQuestions = view.findViewById(R.id.tv_totalQuestions);
        Button button = view.findViewById(R.id.bt_request);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.setEnabled(false);
                if (onWaiting != null) {
                    onWaiting.onRequest();
                }
            }
        });

        tv_inviteName.setText(data.getInviteName());
        try {
            tv_category.setText(data.getCategoryByLanguage(getString(R.string.language)).getCategoryName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_difficulty.setText(data.getDifficulty());
        tv_totalQuestions.setText(String.valueOf(data.getNumberOfQuestions()));

        WaitingPlayersAdapter waitingPlayersAdapter = new WaitingPlayersAdapter(roomBeginningState.getContent().getPlayers());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(waitingPlayersAdapter);

        return view;
    }

    public void setPlayers(List<Player> players) {
        WaitingPlayersAdapter waitingPlayersAdapter = (WaitingPlayersAdapter) recyclerView.getAdapter();
        if (waitingPlayersAdapter != null) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new PlayerDiffUtil(waitingPlayersAdapter.getPlayerList(), players));
            waitingPlayersAdapter.setPlayerList(players);
            getActivity().runOnUiThread(() -> diffResult.dispatchUpdatesTo(waitingPlayersAdapter));
        }
    }
}