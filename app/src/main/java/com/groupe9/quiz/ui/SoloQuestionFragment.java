package com.groupe9.quiz.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.RetrofitService;
import com.groupe9.quiz.webservice.pojo.Question;
import com.groupe9.quiz.webservice.service.SoloGameService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoloQuestionFragment extends Fragment implements QuizAnswerAdapter.OnAnswerListener {

    private static final String SOLO_QUIZ = "solo_quiz";
    private static final String AUTH_TOKEN = "AUTH_TOKEN";
    private static final String DIFFICULTY_ID = "difficulty_id";
    private static final String CATEGORY_ID = "category_id";

    private Activity activity;
    private Question data;
    private int difficultyId;
    private int categoryId;
    private String token = "";
    private LinearLayout ll_content;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TextView bt_question;

    public SoloQuestionFragment() {
        // Required empty public constructor
    }

    public static SoloQuestionFragment newInstance(int difficultyId, int categoryId) {
        SoloQuestionFragment fragment = new SoloQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(DIFFICULTY_ID, difficultyId);
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            data = (Question) savedInstanceState.getSerializable(SOLO_QUIZ);
        }

        if (getArguments() != null) {
            data = (Question) getArguments().getSerializable(SOLO_QUIZ);
            difficultyId = getArguments().getInt(DIFFICULTY_ID, -1);
            categoryId = getArguments().getInt(CATEGORY_ID, -1);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SOLO_QUIZ, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.solo_question_fragment, container, false);
        activity = getActivity();
        bt_question = view.findViewById(R.id.tv_solo_question);
        recyclerView = view.findViewById(R.id.rv_solo_answers);
        ll_content = view.findViewById(R.id.ll_solo_content);
        progressBar = view.findViewById(R.id.pb_solo_question);
        if (activity != null) {
            Bundle extras = activity.getIntent().getExtras();
            if (extras != null) {
                token = extras.getString(AUTH_TOKEN);
            }
        }
        if (data == null) {
            initQuestion();
        } else {
            displayQuestion();
        }
        return view;
    }


    @Override
    public void onClick(String selectedAnswer) {
        activity.runOnUiThread(() -> {
            String value = data.getCorrectAnswer();
            value = value.equals(selectedAnswer) ? getString(R.string.correct_answer) : getString(R.string.incorrect)+"\n" + getString(R.string.correct_answer) + " : " + value;
            Toast.makeText(getContext(), value, Toast.LENGTH_LONG).show();
            getParentFragmentManager().popBackStack();
            getParentFragmentManager().beginTransaction().replace(R.id.container, new SoloAnswerFragment()).addToBackStack(null).commit();
        });
    }

    public void displayQuestion() {
        QuizAnswerAdapter quizAnswerAdapter = new QuizAnswerAdapter(data.getPossibleAnswers());
        quizAnswerAdapter.setOnAnswerListener(this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(quizAnswerAdapter);
        bt_question.setText(data.getQuestion());
        ll_content.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void initQuestion() {
        SoloGameService soloGameService = RetrofitService.createService(SoloGameService.class, token);
        Call<Question> call;
        if (difficultyId == -1 && categoryId == -1) {
            call = soloGameService.getRandomQuestion(getString(R.string.language));
        } else if (difficultyId == -1) {
            call = soloGameService.getRandomQuestionWithCategory(getString(R.string.language), categoryId);
        } else if (categoryId == -1) {
            call = soloGameService.getRandomQuestionWithDifficulty(getString(R.string.language), difficultyId);
        } else {
            call = soloGameService.getRandomQuestion(getString(R.string.language), categoryId, difficultyId);
        }
        call.enqueue(new Callback<Question>() {
            @Override
            public void onResponse(@NonNull Call<Question> call, @NonNull Response<Question> response) {
                if (response.isSuccessful()) {
                    data = response.body();
                    displayQuestion();
                } else {
                    activity.runOnUiThread(() -> {
                        Toast.makeText(getContext(), getString(R.string.question_not_found), Toast.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<Question> call, @NonNull Throwable t) {
                View view = getView();
                if (view != null) {
                    activity.runOnUiThread(() -> {
                        Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }
        });
    }
}