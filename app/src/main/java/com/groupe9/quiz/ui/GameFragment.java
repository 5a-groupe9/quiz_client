package com.groupe9.quiz.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.groupe9.quiz.R;

public class GameFragment extends Fragment {

    private static final String MESSAGE = "message";
    private String message;

    private GameFragment() {

    }

    public static GameFragment newInstance(String message) {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putString(MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(MESSAGE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_main_fragment, container, false);
        TextView textView = view.findViewById(R.id.tv_message_loading);
        textView.setText(message);
        return view;
    }

}