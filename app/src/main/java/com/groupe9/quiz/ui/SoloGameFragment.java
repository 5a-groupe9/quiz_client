package com.groupe9.quiz.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.google.android.material.snackbar.Snackbar;
import com.groupe9.quiz.R;
import com.groupe9.quiz.webservice.RetrofitService;
import com.groupe9.quiz.webservice.pojo.Category;
import com.groupe9.quiz.webservice.pojo.Difficulty;
import com.groupe9.quiz.webservice.service.SoloGameService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoloGameFragment extends Fragment implements View.OnClickListener {

    private static final String AUTH_TOKEN = "AUTH_TOKEN";
    private static final String DIFFICULTY_ID = "difficulty_id";
    private static final String CATEGORY_ID = "category_id";

    private Activity activity;
    private Button bt_submit;
    private Spinner sp_difficulty;
    private Spinner sp_category;
    private String token = "";
    private List<Category> categories;
    private List<Difficulty> difficulties;
    private ArrayAdapter<Category> categoryArrayAdapter;
    private ArrayAdapter<Difficulty> difficultyArrayAdapter;
    private int difficultyId;
    private int categoryId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_solo_fragment, container, false);
        activity = getActivity();
        if (activity != null) {
            Bundle extras = activity.getIntent().getExtras();
            if (extras != null) {
                token = extras.getString(AUTH_TOKEN);
            }
        }
        categories = new ArrayList<>();
        difficulties = new ArrayList<>();
        sp_category = view.findViewById(R.id.sp_category);
        categoryArrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, categories);
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category.setAdapter(categoryArrayAdapter);
        initCategory();

        sp_difficulty = view.findViewById(R.id.sp_difficulty);
        difficultyArrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, difficulties);
        difficultyArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_difficulty.setAdapter(difficultyArrayAdapter);
        initDifficulty();

        bt_submit = view.findViewById(R.id.bt_start_solo);
        bt_submit.setOnClickListener(this);

        return view;
    }

    private void initCategory() {
        SoloGameService soloGameService = RetrofitService.createService(SoloGameService.class, token);
        Call<List<Category>> call = soloGameService.getCategories(getString(R.string.language));
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(@NonNull Call<List<Category>> call, @NonNull Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    categories.clear();
                    List<Category> list = response.body();
                    if (list != null) {
                        Category category = new Category(-1, getString(R.string.random));
                        categories.add(category);
                        categories.addAll(list);
                        activity.runOnUiThread(() -> {
                            categoryArrayAdapter.notifyDataSetChanged();
                        });
                    }
                } else {
                    activity.runOnUiThread(() -> {
                        Toast.makeText(getContext(), getString(R.string.category_not_found), Toast.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Category>> call, @NonNull Throwable t) {
                View view = getView();
                if (view != null) {
                    activity.runOnUiThread(() -> {
                        Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }
        });
    }

    private void initDifficulty() {
        SoloGameService soloGameService = RetrofitService.createService(SoloGameService.class, token);
        Call<List<Difficulty>> call = soloGameService.getDifficulties();
        call.enqueue(new Callback<List<Difficulty>>() {
            @Override
            public void onResponse(@NonNull Call<List<Difficulty>> call, @NonNull Response<List<Difficulty>> response) {
                if (response.isSuccessful()) {
                    difficulties.clear();
                    List<Difficulty> list = response.body();
                    if (list != null) {
                        Difficulty difficulty = new Difficulty(-1, getString(R.string.random));
                        difficulties.add(difficulty);
                        difficulties.addAll(list);
                        activity.runOnUiThread(() -> {
                            difficultyArrayAdapter.notifyDataSetChanged();
                        });
                    }
                } else {
                    activity.runOnUiThread(() -> {
                        Toast.makeText(getContext(), getString(R.string.difficulty_not_found), Toast.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Difficulty>> call, @NonNull Throwable t) {
                View view = getView();
                if (view != null) {
                    activity.runOnUiThread(() -> {
                        Snackbar.make(view, getString(R.string.server_unavailable), Snackbar.LENGTH_LONG).show();
                        activity.finish();
                    });
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == bt_submit) {
            Difficulty difficulty = (Difficulty) sp_difficulty.getSelectedItem();
            difficultyId = difficulty.getIdDifficulty();
            Category category = (Category) sp_category.getSelectedItem();
            categoryId = category.getIdCategory();
            getParentFragmentManager().beginTransaction()
                    .replace(R.id.container, SoloQuestionFragment.newInstance(difficultyId, categoryId))
                    .addToBackStack(null).commit();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            categoryId = savedInstanceState.getInt(CATEGORY_ID);
            difficultyId = savedInstanceState.getInt(DIFFICULTY_ID);
        }
        getParentFragmentManager().setFragmentResultListener("questionKey", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                if (requestKey.equals("questionKey")) {
                    activity.runOnUiThread(() -> {
                        getParentFragmentManager().beginTransaction()
                                .replace(R.id.container, SoloQuestionFragment.newInstance(difficultyId, categoryId)).addToBackStack(null).commit();
                    });
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CATEGORY_ID, categoryId);
        outState.putInt(DIFFICULTY_ID, difficultyId);
    }
}