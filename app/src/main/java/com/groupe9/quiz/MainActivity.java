package com.groupe9.quiz;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.groupe9.quiz.ui.LoginFragment;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new LoginFragment()).commit();
        }
    }

}