package com.groupe9.quiz.webservice.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Player {

    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("levelName")
    @Expose
    private String levelName;
    @SerializedName("totalExp")
    @Expose
    private Integer totalExp;
    @SerializedName("name")
    @Expose
    private String name;

}
