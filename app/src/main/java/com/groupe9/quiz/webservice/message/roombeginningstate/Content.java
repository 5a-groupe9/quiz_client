package com.groupe9.quiz.webservice.message.roombeginningstate;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Content implements Serializable {

    @SerializedName("players")
    @Expose
    private List<Player> players;

    @SerializedName("difficulty")
    @Expose
    private String difficulty;

    @SerializedName("category")
    @Expose
    private List<Category> category;

    @SerializedName("inviteName")
    @Expose
    private String inviteName;

    @SerializedName("responseTime")
    @Expose
    private Integer responseTime;

    @SerializedName("numberOfQuestions")
    @Expose
    private Integer numberOfQuestions;

    @SerializedName("numberOfStartRequest")
    @Expose
    private Integer numberOfStartRequest;

    public Category getCategoryByLanguage(String language) throws Exception {
        return category.stream().filter(l -> l.getLanguage().equals(language)).findFirst().orElseThrow(() -> new Exception("No question found"));
    }

}