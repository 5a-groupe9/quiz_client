package com.groupe9.quiz.webservice.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Category {

    @SerializedName("idCategory")
    @Expose
    private Integer idCategory;

    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    @Override
    public String toString() {
        return categoryName;
    }
}
