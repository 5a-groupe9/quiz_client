package com.groupe9.quiz.webservice.message.roombeginningstate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RoomBeginningState implements Serializable {

    @SerializedName("doesExist")
    @Expose
    private Boolean doesExist;

    @SerializedName("content")
    @Expose
    private Content content;
}
