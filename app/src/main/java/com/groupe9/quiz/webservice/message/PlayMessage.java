package com.groupe9.quiz.webservice.message;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PlayMessage implements Serializable {

    private int idQuestion;
    private String answerSelected;
}
