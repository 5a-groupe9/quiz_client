package com.groupe9.quiz.webservice.service;

import com.groupe9.quiz.model.User;
import com.groupe9.quiz.webservice.pojo.Leaderboard;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PlayerService {

    @POST("/players/createPlayerByUser")
    Call<ResponseBody> createUser(@Body User user); // TODO replace the class returned

    @GET("/players/leaderboard/50")
    Call<Leaderboard> getLeaderBoard();
}
