package com.groupe9.quiz.webservice.message.answer;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.groupe9.quiz.webservice.message.question.QuestionsList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AnswerQuiz implements Serializable {

    @SerializedName("answerList")
    @Expose
    private List<AnswerList> answerList = null;
    @SerializedName("playerAndScore")
    @Expose
    private List<PlayerAndScore> playerAndScore = null;

    public AnswerList getQuestionByLanguage(String language) throws Exception {
        return answerList.stream().filter(l -> l.getLanguage().equals(language)).findFirst().orElseThrow(() -> new Exception("No question found"));
    }
}
