package com.groupe9.quiz.webservice.message.roombeginningstate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class Player implements Serializable {

    @SerializedName("level")
    @Expose
    private Integer level;

    @SerializedName("name")
    @Expose
    private String name;


}
