package com.groupe9.quiz.webservice.service;

import com.groupe9.quiz.webservice.pojo.Category;
import com.groupe9.quiz.webservice.pojo.Difficulty;
import com.groupe9.quiz.webservice.pojo.Question;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SoloGameService {

    @GET("/soloGame/category")
    Call<List<Category>> getCategories(@Query("language") String language);

    @GET("/soloGame/difficulty")
    Call<List<Difficulty>> getDifficulties();

    @GET("/soloGame/question")
    Call<Question> getRandomQuestion(@Query("language") String language, @Query("category") int category, @Query("difficulty") int difficulty);

    @GET("/soloGame/question")
    Call<Question> getRandomQuestion(@Query("language") String language);

    @GET("/soloGame/question")
    Call<Question> getRandomQuestionWithCategory(@Query("language") String language, @Query("category") int category);

    @GET("/soloGame/question")
    Call<Question> getRandomQuestionWithDifficulty(@Query("language") String language, @Query("difficulty") int difficulty);
}
