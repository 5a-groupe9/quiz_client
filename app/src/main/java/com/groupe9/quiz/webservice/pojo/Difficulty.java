package com.groupe9.quiz.webservice.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Difficulty {

    @SerializedName("idDifficulty")
    @Expose
    private Integer idDifficulty;

    @SerializedName("difficultyName")
    @Expose
    private String difficultyName;

    @Override
    public String toString() {
        return difficultyName;
    }
}
