package com.groupe9.quiz.webservice.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Leaderboard {

    @SerializedName("players")
    @Expose
    private List<Player> players = null;

}
