package com.groupe9.quiz.webservice.message.question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QuestionsList implements Serializable {

    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("content")
    @Expose
    private Content content;

}
