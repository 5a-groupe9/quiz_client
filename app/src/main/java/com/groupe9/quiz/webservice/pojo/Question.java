package com.groupe9.quiz.webservice.pojo;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Question implements Serializable {

    @SerializedName("idQuestion")
    @Expose
    private Integer idQuestion;
    @SerializedName("possibleAnswers")
    @Expose
    private List<String> possibleAnswers = null;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("correctAnswer")
    @Expose
    private String correctAnswer;
    @SerializedName("smallDetails")
    @Expose
    private String smallDetails;

}
