package com.groupe9.quiz.webservice.message.question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QuestionQuiz implements Serializable {

    @SerializedName("questionsList")
    @Expose
    private List<QuestionsList> questionsList = null;

    @SerializedName("questionNumber")
    @Expose
    private String questionNumber;

    @SerializedName("questionTotalNumber")
    @Expose
    private String questionTotalNumber;

    public QuestionsList getQuestionByLanguage(String language) throws Exception {
        return questionsList.stream().filter(l -> l.getLanguage().equals(language)).findFirst().orElseThrow(() -> new Exception("No question found"));
    }
}
