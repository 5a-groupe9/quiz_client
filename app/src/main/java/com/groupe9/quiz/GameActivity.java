package com.groupe9.quiz;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.groupe9.quiz.model.WebSocketLifecycleEvent;
import com.groupe9.quiz.model.util.ServerUtil;
import com.groupe9.quiz.ui.GameFragment;
import com.groupe9.quiz.ui.QuestionFragment;
import com.groupe9.quiz.ui.QuizAnswerAdapter;
import com.groupe9.quiz.ui.StateFragment;
import com.groupe9.quiz.ui.WaitingFragment;
import com.groupe9.quiz.webservice.message.PlayMessage;
import com.groupe9.quiz.webservice.message.answer.AnswerQuiz;
import com.groupe9.quiz.webservice.message.question.QuestionQuiz;
import com.groupe9.quiz.webservice.message.question.QuestionsList;
import com.groupe9.quiz.webservice.message.roombeginningstate.RoomBeginningState;

import java.util.Collections;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class GameActivity extends AppCompatActivity implements WebSocketLifecycleEvent.OnErrorListener, QuizAnswerAdapter.OnAnswerListener {

    private static final String ROOM_TAG = "room_tag";
    private static final String STATE_TAG = "state_tag";
    private String token;
    private int responseTime = 0;
    private int currentQuestionId = 0;
    private String inviteName;
    private StompClient stompClient;
    private Gson gson = new Gson();
    private CompositeDisposable compositeDisposable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
        setContentView(R.layout.game_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, GameFragment.newInstance(getString(R.string.loading) + "...")).commitNow();
        }
        Bundle extras = getIntent().getExtras();
        token = (extras != null) ? extras.getString(HomeActivity.AUTH_TOKEN) : "";
        authenticate();
        searchGame();
    }

    private void authenticate() {
        Map<String, String> headerMap = Collections.singletonMap("Authorization", token);
        stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, ServerUtil.SERVER_MAIN_ENDPOINT, headerMap);
        Disposable disposable = stompClient.lifecycle().subscribe(new WebSocketLifecycleEvent(this, this));
        compositeDisposable.add(disposable);
        stompClient.connect();
    }

    private void searchGame() {
        Disposable disposable = stompClient.topic("/user/secured/topic/roomState")
                .subscribe(stompMessage -> {
                    RoomBeginningState roomBeginningState = gson.fromJson(stompMessage.getPayload(), RoomBeginningState.class);
                    if (roomBeginningState.getDoesExist()) {
                        inviteName = roomBeginningState.getContent().getInviteName();
                        subscribeGameEvent(inviteName);
                        responseTime = roomBeginningState.getContent().getResponseTime();
                        runOnUiThread(() -> getSupportFragmentManager().beginTransaction().replace(R.id.container, WaitingFragment.newInstance(roomBeginningState), ROOM_TAG).commitNow());
                    }
                });
        compositeDisposable.add(disposable);
        stompClient.send("/app/room/join").subscribe();
    }

    private void subscribeGameEvent(String inviteName) {
        Disposable disposableNextQuestion = stompClient.topic("/secured/topic/room/" + inviteName + "/nextQuestion")
                .subscribe(stompMessage -> {
                    QuestionQuiz data = gson.fromJson(stompMessage.getPayload(), QuestionQuiz.class);
                    QuestionsList questionsList = data.getQuestionByLanguage(getString(R.string.language));
                    currentQuestionId = questionsList.getContent().getIdQuestion();
                    runOnUiThread(() -> getSupportFragmentManager().beginTransaction().replace(R.id.container, QuestionFragment.newInstance(data, responseTime)).commitNow());
                });

        Disposable disposablePreviousAnswer = stompClient.topic("/secured/topic/room/" + inviteName + "/previousAnswer")
                .subscribe(stompMessage -> {
                    AnswerQuiz data = gson.fromJson(stompMessage.getPayload(), AnswerQuiz.class);
                    runOnUiThread(() -> getSupportFragmentManager().beginTransaction().replace(R.id.container, StateFragment.newInstance(data), STATE_TAG).commitNow());
                });

        Disposable disposableEndGame = stompClient.topic("/secured/topic/room/" + inviteName + "/endGame")
                .subscribe(stompMessage -> {
                    StateFragment fragment = (StateFragment) getSupportFragmentManager().findFragmentByTag(STATE_TAG);
                    if (fragment != null) {
                        fragment.setProgressBar();
                    }
                    runOnUiThread(this::displayEndGame);
                });

        Disposable disposableRoomState = stompClient.topic("/secured/topic/room/" + inviteName + "/roomState")
                .subscribe(stompMessage -> {
                    RoomBeginningState roomBeginningState = gson.fromJson(stompMessage.getPayload(), RoomBeginningState.class);
                    if (roomBeginningState.getDoesExist()) {
                        WaitingFragment fragment = (WaitingFragment) getSupportFragmentManager().findFragmentByTag(ROOM_TAG);
                        if (fragment != null && fragment.isVisible()) {
                            fragment.setPlayers(roomBeginningState.getContent().getPlayers());
                        }
                    }
                });
        compositeDisposable.addAll(disposableNextQuestion, disposablePreviousAnswer, disposableRoomState, disposableEndGame);
    }

    private void displayEndGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.well_played) + "\n" + getString(R.string.quit_request)).setTitle(getString(R.string.game_finished));

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, id) -> finish());

        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, id) -> {
        });
        builder.show();
    }

    @Override
    public void runOnErrorActions() {
        finish();
    }

    @Override
    public void onClick(String selectedAnswer) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, GameFragment.newInstance(getString(R.string.waiting) + "...")).commitNow();
        stompClient.send("/app/room/" + inviteName + "/play", gson.toJson(new PlayMessage(currentQuestionId, selectedAnswer))).subscribe();
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof QuestionFragment) {
            QuestionFragment questionFragment = (QuestionFragment) fragment;
            questionFragment.setOnAnswerListener(this);
        } else if (fragment instanceof WaitingFragment) {
            WaitingFragment waitingFragment = (WaitingFragment) fragment;
            waitingFragment.setOnWaiting(() -> stompClient.send("/app/room/" + inviteName + "/startGame").subscribe());
        }
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed())
            compositeDisposable.dispose();
        super.onDestroy();
    }

}