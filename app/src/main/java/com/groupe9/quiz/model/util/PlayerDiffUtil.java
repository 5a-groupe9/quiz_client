package com.groupe9.quiz.model.util;

import androidx.recyclerview.widget.DiffUtil;

import com.groupe9.quiz.webservice.message.roombeginningstate.Player;

import java.util.List;

public class PlayerDiffUtil extends DiffUtil.Callback {

    private List<Player> oldPlayers;
    private List<Player> newPlayers;

    public PlayerDiffUtil(List<Player> oldPlayers, List<Player> newPlayers) {
        this.oldPlayers = oldPlayers;
        this.newPlayers = newPlayers;
    }

    @Override
    public int getOldListSize() {
        return oldPlayers.size();
    }

    @Override
    public int getNewListSize() {
        return newPlayers.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPlayers.get(oldItemPosition).getName().equals(newPlayers.get(newItemPosition).getName());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPlayers.get(oldItemPosition).equals(newPlayers.get(newItemPosition));
    }

}
