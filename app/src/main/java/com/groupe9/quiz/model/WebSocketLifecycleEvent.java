package com.groupe9.quiz.model;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import io.reactivex.functions.Consumer;
import ua.naiksoftware.stomp.dto.LifecycleEvent;

public class WebSocketLifecycleEvent implements Consumer<LifecycleEvent> {

    private Activity activity;
    private OnErrorListener onErrorListener;

    public WebSocketLifecycleEvent(Activity activity, OnErrorListener onErrorListener) {
        this.activity = activity;
        this.onErrorListener = onErrorListener;
    }

    public interface OnErrorListener {
        void runOnErrorActions();
    }

    @Override
    public void accept(LifecycleEvent lifecycleEvent) {
        switch (lifecycleEvent.getType()) {
            case ERROR:
                activity.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                activity.runOnUiThread(() -> {
                    if (onErrorListener != null) onErrorListener.runOnErrorActions();
                });
                handleLifecycleException(lifecycleEvent.getException());
                break;
            case OPENED:
                //TODO access main page
                break;
            default:
                break;
        }
    }

    private void toast(String message) {
        activity.runOnUiThread(() -> Toast.makeText(activity, message, Toast.LENGTH_LONG).show());
    }

    private void handleLifecycleException(Exception e) {
        String error = e.getMessage();
        if (error == null) {
            return;
        }
        Log.d("WEBSOCKET", " " + error);
        if (error.contains("ProtocolException")) {
            toast("Bad credentials or user unauthorized");
        } else if (error.contains("SocketException")) {
            toast("Error, you are disconnected");
        } else if (error.contains("SocketTimeoutException")) {
            toast("Failed to connect");
        } else {
            toast("Error");
        }
    }

}
