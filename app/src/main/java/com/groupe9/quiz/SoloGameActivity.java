package com.groupe9.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.groupe9.quiz.ui.SoloGameFragment;

public class SoloGameActivity extends AppCompatActivity {

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.solo_game_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new SoloGameFragment()).commitNow();
        }
        Bundle extras = getIntent().getExtras();
        token = (extras != null) ? extras.getString(HomeActivity.AUTH_TOKEN) : "";
    }

}